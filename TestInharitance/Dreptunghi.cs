﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInharitance
{
    class Dreptunghi : Shape
    {

        private double lungime, inaltime;

        public Dreptunghi(double l, double i)
        {
            this.lungime = l;
            this.inaltime = i;
        }

        public double Lungime
        {
            get { return this.lungime; }
            set { this.lungime = value; }
        }

        public double Inaltime
        {
            get { return this.inaltime; }
            set { this.inaltime = value; }
        }

        public override double Arie()
        {
            return this.lungime * this.inaltime;
        }

        public override double Perimetru()
        {
            return 2 * (this.lungime + this.inaltime);
        }
    }
}
