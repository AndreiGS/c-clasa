﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestInharitance
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        Shape[] shapes = new Shape[50];
        int nrShapes = 0;
        private void submit_Click(object sender, EventArgs e)
        {
            Shape newShape;
            if(Forme.SelectedItem.Equals("Cerc"))
            {
                newShape = new Cerc(Convert.ToDouble(Raza.Text));
                shapes[nrShapes++] = newShape;
            }
            else if (Forme.SelectedItem.Equals("Patrat"))
            {
                newShape = new Patrat(Convert.ToDouble(Lungime.Text));
                shapes[nrShapes++] = newShape;

            }
            else if (Forme.SelectedItem.Equals("Dreptunghi"))
            {
                newShape = new Dreptunghi(Convert.ToDouble(Lungime.Text),
                                        Convert.ToDouble(Inaltime.Text));
                shapes[nrShapes++] = newShape;
            }
        }

        private void Forme_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Forme.SelectedItem.Equals("Cerc"))
            {
                Raza.Enabled = true;
                Lungime.Enabled = false;
                Inaltime.Enabled = false;
                Raza.Clear();
                Lungime.Clear();
                Inaltime.Clear();
            }
            else if (Forme.SelectedItem.Equals("Patrat"))
            {
                Lungime.Enabled = true;
                Inaltime.Enabled = false;
                Raza.Enabled = false;
                Raza.Clear();
                Lungime.Clear();
                Inaltime.Clear();
            }
            else if (Forme.SelectedItem.Equals("Dreptunghi"))
            {
                Lungime.Enabled = true;
                Inaltime.Enabled = true;
                Raza.Enabled = false;
                Raza.Clear();
                Lungime.Clear();
                Inaltime.Clear();
            }
        }

        private void afiseaza_Click(object sender, EventArgs e)
        {
            /*foreach(Shape shape in shapes)
            {
                if(shape == NULL)
                    break;
                MessageBox.Show(shape.Arie().ToString());
            }*/
            for (int i = 0; i < nrShapes; i++)
            {
                if(shapes[i].GetType() == typeof(Circle))
                    MessageBox.Show("Circle");
                MessageBox.Show("Arie: " + shapes[i].Arie().ToString() + '\n' + 
                                "Perimetru: " + shapes[i].Perimetru().ToString());
            }
        }
    }
}
