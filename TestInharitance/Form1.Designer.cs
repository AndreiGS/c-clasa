﻿namespace TestInharitance
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Forme = new System.Windows.Forms.ComboBox();
            this.Lungime = new System.Windows.Forms.TextBox();
            this.Raza = new System.Windows.Forms.TextBox();
            this.Inaltime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.submit = new System.Windows.Forms.Button();
            this.afiseaza = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Forme
            // 
            this.Forme.FormattingEnabled = true;
            this.Forme.Items.AddRange(new object[] {
            "Dreptunghi",
            "Patrat",
            "Cerc"});
            this.Forme.Location = new System.Drawing.Point(119, 12);
            this.Forme.Name = "Forme";
            this.Forme.Size = new System.Drawing.Size(123, 21);
            this.Forme.TabIndex = 0;
            this.Forme.SelectedIndexChanged += new System.EventHandler(this.Forme_SelectedIndexChanged);
            // 
            // Lungime
            // 
            this.Lungime.Enabled = false;
            this.Lungime.Location = new System.Drawing.Point(119, 57);
            this.Lungime.Name = "Lungime";
            this.Lungime.Size = new System.Drawing.Size(123, 20);
            this.Lungime.TabIndex = 1;
            // 
            // Raza
            // 
            this.Raza.Enabled = false;
            this.Raza.Location = new System.Drawing.Point(119, 149);
            this.Raza.Name = "Raza";
            this.Raza.Size = new System.Drawing.Size(123, 20);
            this.Raza.TabIndex = 2;
            // 
            // Inaltime
            // 
            this.Inaltime.Enabled = false;
            this.Inaltime.Location = new System.Drawing.Point(119, 106);
            this.Inaltime.Name = "Inaltime";
            this.Inaltime.Size = new System.Drawing.Size(123, 20);
            this.Inaltime.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Forma:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Lungime:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Inaltime:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 149);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Raza:";
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(119, 187);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(91, 23);
            this.submit.TabIndex = 8;
            this.submit.Text = "Adauga";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // afiseaza
            // 
            this.afiseaza.Location = new System.Drawing.Point(30, 187);
            this.afiseaza.Name = "afiseaza";
            this.afiseaza.Size = new System.Drawing.Size(70, 23);
            this.afiseaza.TabIndex = 9;
            this.afiseaza.Text = "Afiseaza";
            this.afiseaza.UseVisualStyleBackColor = true;
            this.afiseaza.Click += new System.EventHandler(this.afiseaza_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.afiseaza);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Inaltime);
            this.Controls.Add(this.Raza);
            this.Controls.Add(this.Lungime);
            this.Controls.Add(this.Forme);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Forme;
        private System.Windows.Forms.TextBox Lungime;
        private System.Windows.Forms.TextBox Raza;
        private System.Windows.Forms.TextBox Inaltime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Button afiseaza;
    }
}

