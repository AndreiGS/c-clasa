﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInharitance
{
    class Patrat : Shape
    {

        public Patrat(double l)
        {
            this.latura = l;
        }

        private double latura;

        public double Latura
        {
            get { return this.latura; }
            set { this.latura = value; }
        }
        public override double Arie()
        {
            return this.latura * this.latura;
        }

        public override double Perimetru()
        {
            return 4 * this.latura;
        }
    }
}
