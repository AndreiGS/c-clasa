﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInharitance
{
    class Cerc : Shape
    {

        private double raza;

        public Cerc(double r)
        {
            this.raza = r;
        }

        public double Raza 
        {
            get { return this.raza; }
            set { this.raza = value; }
        }

        public override double Arie()
        {
            return Math.PI * this.raza * this.raza;
        }

        public override double Perimetru()
        {
            return 2 * Math.PI * this.raza;
        }


    }
}
