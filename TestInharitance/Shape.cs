﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInharitance
{
    abstract class Shape
    {
        abstract public double Arie();
        abstract public double Perimetru();
    }
}
