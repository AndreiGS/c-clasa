﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProiectInheritance
{
    //de vorbit: ierarhie, shape shape = new circle != circle circle = new circle, list<Shape>


    public partial class Form1 : Form
    {
        int initialHeight = 294+50, initialWidth = 209;
        string shape;
        Graphics g;

        private void resetTextBoxes()
        {
            textBoxRadius.Text = "";
            textBoxHeight.Text = "";
            textBoxWidth.Text = "";
            textBoxLowerSide.Text = "";
            textBoxRightSide.Text = "";
            textBoxLeftSide.Text = "";
        }

        private void comboBoxShape_SelectedIndexChanged(object sender, EventArgs e)
        {
            shape = comboBoxShape.Text;
            if (shape.Equals("Circle"))
            {
                textBoxRadius.Enabled = true;
                textBoxHeight.Enabled = false;
                textBoxWidth.Enabled = false;
                textBoxLowerSide.Enabled = false;
                textBoxRightSide.Enabled = false;
                textBoxLeftSide.Enabled = false;

                resetTextBoxes();
            }
            else if (shape.Equals("Triangle"))
            {
                textBoxRadius.Enabled = false;
                textBoxHeight.Enabled = false;
                textBoxWidth.Enabled = false;
                textBoxLowerSide.Enabled = true;
                textBoxRightSide.Enabled = true;
                textBoxLeftSide.Enabled = true;

                resetTextBoxes();
            }
            else if (shape.Equals("Rectangle"))
            {
                textBoxRadius.Enabled = false;
                textBoxHeight.Enabled = true;
                textBoxWidth.Enabled = true;
                textBoxLowerSide.Enabled = false;
                textBoxRightSide.Enabled = false;
                textBoxLeftSide.Enabled = false;

                resetTextBoxes();
            }
            else if (shape.Equals("Square"))
            {
                textBoxRadius.Enabled = false;
                textBoxHeight.Enabled = true;
                textBoxWidth.Enabled = false;
                textBoxLowerSide.Enabled = false;
                textBoxRightSide.Enabled = false;
                textBoxLeftSide.Enabled = false;

                resetTextBoxes();
            }
        }

        private void buttonDraw_Paint()
        {
            
        }

        public static int checkTriangleValidity(int a, int b, int c)
        {
            if (a + b <= c || a + c <= b || b + c <= a)
                return 0;
            else
                return 1;
        }

        private void drawShape()
        {
            g = base.CreateGraphics();

            if (shape == null)
                return;

            if (shape.Equals("Circle"))
            {
                this.Width = initialWidth + 20 + Convert.ToInt32(textBoxRadius.Text) * 2 + 20;

                if (initialHeight < Convert.ToInt32(textBoxRadius.Text) + 50)
                    this.Height = initialHeight + 20 + Convert.ToInt32(textBoxRadius.Text) * 2 + 20;

                g.Clear(Color.WhiteSmoke);
                g.DrawEllipse(new Pen(Color.Black), initialWidth, this.Height / 2 - Convert.ToInt32(textBoxRadius.Text), Convert.ToInt32(textBoxRadius.Text) * 2, Convert.ToInt32(textBoxRadius.Text) * 2);
            }
            else if (shape.Equals("Triangle"))
            {
                if (checkTriangleValidity(Convert.ToInt32(textBoxLowerSide.Text), Convert.ToInt32(textBoxRightSide.Text), Convert.ToInt32(textBoxLeftSide.Text)) == 1)
                {
                    this.Width = initialWidth + 20 + Convert.ToInt32(textBoxLowerSide.Text) * 2 + 20;

                    if (initialHeight < Convert.ToInt32(textBoxLeftSide.Text) + 50)
                        this.Height = initialHeight + 20 + Convert.ToInt32(textBoxLeftSide.Text) + 20;

                    int xC = initialWidth + (int)(Math.Abs(Convert.ToInt32(textBoxLeftSide.Text) * Convert.ToInt32(textBoxLeftSide.Text) - Convert.ToInt32(textBoxRightSide.Text) * Convert.ToInt32(textBoxRightSide.Text)) - 2 * Convert.ToInt32(textBoxLowerSide.Text)) / Convert.ToInt32(textBoxLowerSide.Text) * Convert.ToInt32(textBoxLowerSide.Text);
                    int yC = 50 + (int)Math.Sqrt(Convert.ToInt32(textBoxLeftSide.Text) * Convert.ToInt32(textBoxLeftSide.Text) - xC * xC);
                    Point[] points = {
                        new Point(initialWidth, 50),
                        new Point(initialWidth + Convert.ToInt32(textBoxLowerSide.Text), 50),
                        new Point(xC, yC)
                    };

                    g.Clear(Color.WhiteSmoke);
                    try
                    {
                        g.DrawPolygon(new Pen(Color.Black), points);
                    }
                    catch (Exception ex)
                    {

                    }

                }
                else
                    MessageBox.Show("Your values are not valid");
            }
            else if (shape.Equals("Rectangle"))
            {
                this.Width = initialWidth + 20 + Convert.ToInt32(textBoxHeight.Text) + 20;

                if (initialHeight < Convert.ToInt32(textBoxHeight.Text) + 50)
                    this.Height = initialHeight + 20 + Convert.ToInt32(textBoxHeight.Text) + 20;

                g.Clear(Color.WhiteSmoke);
                g.DrawRectangle(new Pen(Color.Black), initialWidth, this.Height / 2 - Convert.ToInt32(textBoxHeight.Text), Convert.ToInt32(textBoxWidth.Text), Convert.ToInt32(textBoxHeight.Text));
            }
            else if (shape.Equals("Square"))
            {
                this.Width = initialWidth + 20 + Convert.ToInt32(textBoxHeight.Text) + 20;

                if (initialHeight < Convert.ToInt32(textBoxHeight.Text) + 50)
                    this.Height = initialHeight + 20 + Convert.ToInt32(textBoxHeight.Text) + 20;

                g.Clear(Color.WhiteSmoke);
                g.DrawRectangle(new Pen(Color.Black), initialWidth, this.Height / 2 - Convert.ToInt32(textBoxHeight.Text), Convert.ToInt32(textBoxHeight.Text), Convert.ToInt32(textBoxHeight.Text));
            }
        }

        private void calculate()
        {
            if(shape.Equals("Circle"))
            {
                Circle circle = new Circle();
                circle.Radius = Convert.ToInt32(textBoxRadius.Text);

                MessageBox.Show($"Circle area is: {circle.calculateArea().ToString()}");

                return;
            } 
            else if(shape.Equals("Rectangle"))
            {

            }
            else if (shape.Equals("Square"))
            {

            }
            else if (shape.Equals("Triangle"))
            {

            }
        }

        private void buttonDraw_Click(object sender, EventArgs e)
        {
            drawShape();
            calculate();
        }

        private void Form1_Load(object sender, EventArgs e)
        { 
        }

        public Form1()
        {
            InitializeComponent();
        }
    }
}
