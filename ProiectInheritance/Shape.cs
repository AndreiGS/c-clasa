﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectInheritance
{
    public abstract class Shape
    {
        public abstract double calculateArea();
    }
}
