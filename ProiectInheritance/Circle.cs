﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectInheritance
{
    class Circle : Shape
    {
        private int radius;
        public int Radius
        {
            get { return radius; }
            set { radius = value; }
        }
        public override double calculateArea()
        {
            return Math.PI * radius * radius;
        }
    }
}
