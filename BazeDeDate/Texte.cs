﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BazeDeDate
{
    public partial class Texte : Form
    {
        SqlConnection con;
        SqlCommand cmd;
        int idText;

        public Texte()
        {
            InitializeComponent();
            con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Andrei\Documents\Notite.mdf;Integrated Security=True;Connect Timeout=30");
        }

        private void AdaugareNotite_Load(object sender, EventArgs e)
        {
        }

        private void prelucrariToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string title = textBoxTitle.Text;
            string text = richTextBox.Text;

            con.Open();

            cmd = new SqlCommand("INSERT INTO Texte(Text, IdUtilizator, Titlu) VALUES (@Text, @IdUtilizator, @Titlu)", con);
            cmd.Parameters.AddWithValue("Text", text);
            cmd.Parameters.AddWithValue("IdUtilizator", Form1.id);
            cmd.Parameters.AddWithValue("Titlu", title);

            cmd.ExecuteNonQuery();

            MessageBox.Show("Page created successfully!");
            textBoxTitle.Clear();
            richTextBox.Clear();

            con.Close();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            con.Open();
            string nume = textBoxTitleToRemove.Text;

            cmd = new SqlCommand("DELETE FROM Texte WHERE Titlu=@Title AND IdUtilizator=@IdUtilizator", con);
            cmd.Parameters.AddWithValue("Title", nume);
            cmd.Parameters.AddWithValue("IdUtilizator", Form1.id);
            int cateSterse = cmd.ExecuteNonQuery();
            cmd.Dispose();

            if(cateSterse==0)
            {
                MessageBox.Show("Nu am gasit textul");
            }
            else
            {
                MessageBox.Show("Am sters textul");
                textBoxTitleToRemove.Clear();
            }

            con.Close();
        }

        private void addTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = tabPageAdd;
        }

        private void removeTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = tabPageRemove;
        }

        private void vizualizareTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = tabPageView;
        }

        private void editareTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl.SelectedTab = tabPageEdit;
        }

        private void buttonCauta_Click(object sender, EventArgs e)
        {
            con.Open();

            string titlu = textBoxTitleToEdit.Text;

            cmd = new SqlCommand("SELECT Text, Id FROM Texte WHERE Titlu=@Titlu AND IdUtilizator=@IdUtilizator", con);
            cmd.Parameters.AddWithValue("Titlu", titlu);
            cmd.Parameters.AddWithValue("IdUtilizator", Form1.id);

            string text = "";
            using(SqlDataReader read = cmd.ExecuteReader())
            {
                while (read.Read())
                {
                    text = read["Text"].ToString();
                    idText = Convert.ToInt32(read["Id"]);
                }
            }

            richTextBoxEdit.Text = text;
            richTextBoxEdit.Visible = true;
            buttonCauta.Enabled = false;
            buttonModifica.Enabled = true;

            con.Close();
        }

        private void buttonModifica_Click(object sender, EventArgs e)
        {
            con.Open();

            string titlu = textBoxTitleToEdit.Text;
            string text = richTextBoxEdit.Text;

            cmd = new SqlCommand("UPDATE Texte SET Text=@Text, Titlu=@Titlu WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("Id", idText);
            cmd.Parameters.AddWithValue("Text", text);
            cmd.Parameters.AddWithValue("Titlu", titlu);
            int nrModificate = cmd.ExecuteNonQuery();

            if(nrModificate==0)
                MessageBox.Show("Nu am gasit notita");
            else
            {
                MessageBox.Show("Notita modificata");
                richTextBoxEdit.Visible = false;
                buttonCauta.Enabled = true;
                buttonModifica.Enabled = false;
                richTextBoxEdit.Clear();
                textBoxTitleToEdit.Clear();
            }

            con.Close();
        }

        private void deleleAllItemsInDataGridView()
        {
            dataGridViewText.Rows.Clear();
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tabControl.SelectedTab==tabPageView)
            {
                deleleAllItemsInDataGridView();
                con.Open();

                cmd = new SqlCommand("SELECT Titlu, Text FROM Texte WHERE IdUtilizator=@IdUtilizator", con);
                cmd.Parameters.AddWithValue("IdUtilizator", Form1.id);

                SqlDataReader read = cmd.ExecuteReader();
                while(read.Read())
                {
                    dataGridViewText.Rows.Add(read["Titlu"].ToString(), read["Text"].ToString());
                }

                con.Close();
            }
        }
    }
}
