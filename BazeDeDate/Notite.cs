﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BazeDeDate
{
    public partial class Notite : Form
    {
        RichTextBox[] texte = new RichTextBox[20];
        TabPage[] taburi = new TabPage[20];
        int nrTexte = 0, nrTaburi;
        string titlu="";
        string text="";

        public Notite()
        {
            InitializeComponent();
        }

        private void FormNotite_Load(object sender, EventArgs e)
        {
            TabPage tp = new TabPage("Test");
            tabControl1.TabPages.Add(tp);
            taburi[nrTaburi++] = tp;

            RichTextBox tb = new RichTextBox();
            tb.Name = "tb1";
            tb.Dock = DockStyle.Fill;
            texte[nrTexte++] = tb;

            tp.Controls.Add(tb);
        }

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonModifica_Click(object sender, EventArgs e)
        {
            titlu=tabControl1.SelectedTab.Text;
            for(int i=0; i<nrTaburi; i++)
            {
                int tabIndex = tabControl1.SelectedIndex;
                foreach (Control c in tabControl1.TabPages[tabIndex].Controls)
                    if (c is RichTextBox)
                        text = c.Text;
            }

            MessageBox.Show(titlu + " "+ text);
        }
    }
}
