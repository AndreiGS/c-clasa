﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BazeDeDate
{
    //CUM SE CREEAZA O BAZA DE DATE
    //CUM SE CREEAZA TABELELE => autoincrement
    //                        => foreign key
    //CONECTARE LA BAZA DE DATE
    //SELECT FOLOSIND WHERE 
    //INSERT

    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd; 
        public static int id;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Andrei\Documents\Notite.mdf;Integrated Security=True;Connect Timeout=30");
        }

        private void buttonNumeInregistrare_Click(object sender, EventArgs e)
        {
            string nume = textBoxNumeInregistrare.Text;
            string parola = textBoxParolaInregistrare.Text;

            con.Open();

            //select utilizatori in functie de nume
            cmd = new SqlCommand("SELECT Id FROM Utilizatori WHERE Nume=@Nume", con);
            cmd.Parameters.AddWithValue("Nume", nume);
            int id = Convert.ToInt32(cmd.ExecuteScalar());

            //verificam daca exista un utilizator cu numele citit
            //daca exista => dam mesaj
            if (id != 0)
            {
                MessageBox.Show("Utilizator deja existent");
                textBoxNumeInregistrare.Clear();
                textBoxParolaInregistrare.Clear();
                con.Close();
                return;
            }

            if (parola.Length < 8)
            {
                MessageBox.Show("Parola e prea scurta");
                con.Close();
                return;
            }

            //daca nu exista => creem utilizatorul => dam mesaj
            cmd = new SqlCommand("INSERT INTO Utilizatori(Nume, Parola) VALUES (@Nume, @Parola)", con);
            cmd.Parameters.AddWithValue("Nume", nume);
            cmd.Parameters.AddWithValue("Parola", parola);
            cmd.ExecuteNonQuery();

            MessageBox.Show("Bine ai venit!");
            textBoxNumeInregistrare.Clear();
            textBoxParolaInregistrare.Clear();

            con.Close();
        }

        private void buttonNumeConectare_Click(object sender, EventArgs e)
        {
            con.Open();

            string nume = textBoxNumeConectare.Text;
            string parola = textBoxParolaConectare.Text;

            //selectam utilizatorul in functie de nume
            cmd = new SqlCommand("SELECT Id FROM Utilizatori WHERE Nume=@Nume", con);
            cmd.Parameters.AddWithValue("Nume", nume);
            id = Convert.ToInt32(cmd.ExecuteScalar());

            //daca nu exista => il punem sa reintroduca datele
            if(id == 0)
            {
                MessageBox.Show("Utilizator gresit!");
                textBoxNumeConectare.Clear();
                textBoxParolaConectare.Clear();
                con.Close();
                return;
            }

            //daca exista => verificam parola => il lasam sa intre
            cmd = new SqlCommand("SELECT Parola FROM Utilizatori WHERE Nume=@Nume", con);
            cmd.Parameters.AddWithValue("Nume", nume);
            string parolaDB = Convert.ToString(cmd.ExecuteScalar());

            if(!parolaDB.Equals(parola))
            {
                MessageBox.Show("Parola gresita!");
                textBoxNumeConectare.Clear();
                textBoxParolaConectare.Clear();
                con.Close();
                return;
            }

            Texte f = new Texte();
            this.Hide();
            f.ShowDialog();
            f.Show();

            textBoxNumeConectare.Clear();
            textBoxParolaConectare.Clear();

            con.Close();
        }
    }

    
}
