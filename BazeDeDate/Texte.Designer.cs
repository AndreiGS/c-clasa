﻿namespace BazeDeDate
{
    partial class Texte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.prelucrariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vizualizareTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editareTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageAdd = new System.Windows.Forms.TabPage();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.tabPageRemove = new System.Windows.Forms.TabPage();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.textBoxTitleToRemove = new System.Windows.Forms.TextBox();
            this.tabPageView = new System.Windows.Forms.TabPage();
            this.tabPageEdit = new System.Windows.Forms.TabPage();
            this.buttonModifica = new System.Windows.Forms.Button();
            this.textBoxTitleToEdit = new System.Windows.Forms.TextBox();
            this.buttonCauta = new System.Windows.Forms.Button();
            this.richTextBoxEdit = new System.Windows.Forms.RichTextBox();
            this.dataGridViewText = new System.Windows.Forms.DataGridView();
            this.TitleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TextColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageAdd.SuspendLayout();
            this.tabPageRemove.SuspendLayout();
            this.tabPageView.SuspendLayout();
            this.tabPageEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewText)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prelucrariToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // prelucrariToolStripMenuItem
            // 
            this.prelucrariToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTextToolStripMenuItem,
            this.removeTextToolStripMenuItem,
            this.vizualizareTextToolStripMenuItem,
            this.editareTextToolStripMenuItem});
            this.prelucrariToolStripMenuItem.Name = "prelucrariToolStripMenuItem";
            this.prelucrariToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.prelucrariToolStripMenuItem.Text = "Prelucrari";
            this.prelucrariToolStripMenuItem.Click += new System.EventHandler(this.prelucrariToolStripMenuItem_Click);
            // 
            // addTextToolStripMenuItem
            // 
            this.addTextToolStripMenuItem.Name = "addTextToolStripMenuItem";
            this.addTextToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addTextToolStripMenuItem.Text = "Add text";
            this.addTextToolStripMenuItem.Click += new System.EventHandler(this.addTextToolStripMenuItem_Click);
            // 
            // removeTextToolStripMenuItem
            // 
            this.removeTextToolStripMenuItem.Name = "removeTextToolStripMenuItem";
            this.removeTextToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.removeTextToolStripMenuItem.Text = "Remove text";
            this.removeTextToolStripMenuItem.Click += new System.EventHandler(this.removeTextToolStripMenuItem_Click);
            // 
            // vizualizareTextToolStripMenuItem
            // 
            this.vizualizareTextToolStripMenuItem.Name = "vizualizareTextToolStripMenuItem";
            this.vizualizareTextToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.vizualizareTextToolStripMenuItem.Text = "Vizualizare Text";
            this.vizualizareTextToolStripMenuItem.Click += new System.EventHandler(this.vizualizareTextToolStripMenuItem_Click);
            // 
            // editareTextToolStripMenuItem
            // 
            this.editareTextToolStripMenuItem.Name = "editareTextToolStripMenuItem";
            this.editareTextToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.editareTextToolStripMenuItem.Text = "Editare Text";
            this.editareTextToolStripMenuItem.Click += new System.EventHandler(this.editareTextToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageAdd);
            this.tabControl.Controls.Add(this.tabPageRemove);
            this.tabControl.Controls.Add(this.tabPageView);
            this.tabControl.Controls.Add(this.tabPageEdit);
            this.tabControl.Location = new System.Drawing.Point(12, 27);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(776, 411);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPageAdd
            // 
            this.tabPageAdd.Controls.Add(this.buttonAdd);
            this.tabPageAdd.Controls.Add(this.textBoxTitle);
            this.tabPageAdd.Controls.Add(this.labelName);
            this.tabPageAdd.Controls.Add(this.richTextBox);
            this.tabPageAdd.Location = new System.Drawing.Point(4, 22);
            this.tabPageAdd.Name = "tabPageAdd";
            this.tabPageAdd.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAdd.Size = new System.Drawing.Size(768, 385);
            this.tabPageAdd.TabIndex = 0;
            this.tabPageAdd.Text = "ADD";
            this.tabPageAdd.UseVisualStyleBackColor = true;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(580, 45);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 4;
            this.buttonAdd.Text = "Submit";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(510, 19);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(224, 20);
            this.textBoxTitle.TabIndex = 3;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(601, 3);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Name";
            // 
            // richTextBox
            // 
            this.richTextBox.Location = new System.Drawing.Point(0, 0);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(480, 385);
            this.richTextBox.TabIndex = 1;
            this.richTextBox.Text = "";
            // 
            // tabPageRemove
            // 
            this.tabPageRemove.Controls.Add(this.buttonRemove);
            this.tabPageRemove.Controls.Add(this.textBoxTitleToRemove);
            this.tabPageRemove.Location = new System.Drawing.Point(4, 22);
            this.tabPageRemove.Name = "tabPageRemove";
            this.tabPageRemove.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRemove.Size = new System.Drawing.Size(768, 385);
            this.tabPageRemove.TabIndex = 1;
            this.tabPageRemove.Text = "REMOVE";
            this.tabPageRemove.UseVisualStyleBackColor = true;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(6, 32);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(203, 37);
            this.buttonRemove.TabIndex = 1;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // textBoxTitleToRemove
            // 
            this.textBoxTitleToRemove.Location = new System.Drawing.Point(6, 6);
            this.textBoxTitleToRemove.Name = "textBoxTitleToRemove";
            this.textBoxTitleToRemove.Size = new System.Drawing.Size(203, 20);
            this.textBoxTitleToRemove.TabIndex = 0;
            // 
            // tabPageView
            // 
            this.tabPageView.Controls.Add(this.dataGridViewText);
            this.tabPageView.Location = new System.Drawing.Point(4, 22);
            this.tabPageView.Name = "tabPageView";
            this.tabPageView.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageView.Size = new System.Drawing.Size(768, 385);
            this.tabPageView.TabIndex = 2;
            this.tabPageView.Text = "VIEW";
            this.tabPageView.UseVisualStyleBackColor = true;
            // 
            // tabPageEdit
            // 
            this.tabPageEdit.Controls.Add(this.buttonModifica);
            this.tabPageEdit.Controls.Add(this.textBoxTitleToEdit);
            this.tabPageEdit.Controls.Add(this.buttonCauta);
            this.tabPageEdit.Controls.Add(this.richTextBoxEdit);
            this.tabPageEdit.Location = new System.Drawing.Point(4, 22);
            this.tabPageEdit.Name = "tabPageEdit";
            this.tabPageEdit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEdit.Size = new System.Drawing.Size(768, 385);
            this.tabPageEdit.TabIndex = 3;
            this.tabPageEdit.Text = "EDIT";
            this.tabPageEdit.UseVisualStyleBackColor = true;
            // 
            // buttonModifica
            // 
            this.buttonModifica.Enabled = false;
            this.buttonModifica.Location = new System.Drawing.Point(6, 356);
            this.buttonModifica.Name = "buttonModifica";
            this.buttonModifica.Size = new System.Drawing.Size(331, 23);
            this.buttonModifica.TabIndex = 3;
            this.buttonModifica.Text = "Modifica";
            this.buttonModifica.UseVisualStyleBackColor = true;
            this.buttonModifica.Click += new System.EventHandler(this.buttonModifica_Click);
            // 
            // textBoxTitleToEdit
            // 
            this.textBoxTitleToEdit.Location = new System.Drawing.Point(19, 6);
            this.textBoxTitleToEdit.Name = "textBoxTitleToEdit";
            this.textBoxTitleToEdit.Size = new System.Drawing.Size(300, 20);
            this.textBoxTitleToEdit.TabIndex = 2;
            // 
            // buttonCauta
            // 
            this.buttonCauta.Location = new System.Drawing.Point(125, 32);
            this.buttonCauta.Name = "buttonCauta";
            this.buttonCauta.Size = new System.Drawing.Size(75, 23);
            this.buttonCauta.TabIndex = 1;
            this.buttonCauta.Text = "Cauta";
            this.buttonCauta.UseVisualStyleBackColor = true;
            this.buttonCauta.Click += new System.EventHandler(this.buttonCauta_Click);
            // 
            // richTextBoxEdit
            // 
            this.richTextBoxEdit.Location = new System.Drawing.Point(343, 6);
            this.richTextBoxEdit.Name = "richTextBoxEdit";
            this.richTextBoxEdit.Size = new System.Drawing.Size(419, 373);
            this.richTextBoxEdit.TabIndex = 0;
            this.richTextBoxEdit.Text = "";
            this.richTextBoxEdit.Visible = false;
            // 
            // dataGridViewText
            // 
            this.dataGridViewText.AllowUserToDeleteRows = false;
            this.dataGridViewText.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewText.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TitleColumn,
            this.TextColumn});
            this.dataGridViewText.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewText.Name = "dataGridViewText";
            this.dataGridViewText.ReadOnly = true;
            this.dataGridViewText.Size = new System.Drawing.Size(768, 385);
            this.dataGridViewText.TabIndex = 0;
            // 
            // TitleColumn
            // 
            this.TitleColumn.HeaderText = "Title";
            this.TitleColumn.Name = "TitleColumn";
            this.TitleColumn.ReadOnly = true;
            this.TitleColumn.Width = 200;
            // 
            // TextColumn
            // 
            this.TextColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TextColumn.HeaderText = "Text";
            this.TextColumn.Name = "TextColumn";
            this.TextColumn.ReadOnly = true;
            // 
            // Texte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Texte";
            this.Text = "AdaugareNotite";
            this.Load += new System.EventHandler(this.AdaugareNotite_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageAdd.ResumeLayout(false);
            this.tabPageAdd.PerformLayout();
            this.tabPageRemove.ResumeLayout(false);
            this.tabPageRemove.PerformLayout();
            this.tabPageView.ResumeLayout(false);
            this.tabPageEdit.ResumeLayout(false);
            this.tabPageEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewText)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem prelucrariToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vizualizareTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editareTextToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageAdd;
        private System.Windows.Forms.TabPage tabPageRemove;
        private System.Windows.Forms.TabPage tabPageView;
        private System.Windows.Forms.TabPage tabPageEdit;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.TextBox textBoxTitleToRemove;
        private System.Windows.Forms.Button buttonModifica;
        private System.Windows.Forms.TextBox textBoxTitleToEdit;
        private System.Windows.Forms.Button buttonCauta;
        private System.Windows.Forms.RichTextBox richTextBoxEdit;
        private System.Windows.Forms.DataGridView dataGridViewText;
        private System.Windows.Forms.DataGridViewTextBoxColumn TitleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TextColumn;
    }
}