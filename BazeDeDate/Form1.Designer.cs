﻿namespace BazeDeDate
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNumeInregistrare = new System.Windows.Forms.TextBox();
            this.buttonNumeInregistrare = new System.Windows.Forms.Button();
            this.buttonNumeConectare = new System.Windows.Forms.Button();
            this.textBoxNumeConectare = new System.Windows.Forms.TextBox();
            this.textBoxParolaInregistrare = new System.Windows.Forms.TextBox();
            this.textBoxParolaConectare = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxNumeInregistrare
            // 
            this.textBoxNumeInregistrare.Location = new System.Drawing.Point(12, 12);
            this.textBoxNumeInregistrare.Name = "textBoxNumeInregistrare";
            this.textBoxNumeInregistrare.Size = new System.Drawing.Size(176, 20);
            this.textBoxNumeInregistrare.TabIndex = 0;
            // 
            // buttonNumeInregistrare
            // 
            this.buttonNumeInregistrare.Location = new System.Drawing.Point(12, 63);
            this.buttonNumeInregistrare.Name = "buttonNumeInregistrare";
            this.buttonNumeInregistrare.Size = new System.Drawing.Size(176, 23);
            this.buttonNumeInregistrare.TabIndex = 3;
            this.buttonNumeInregistrare.Text = "Inregistrare";
            this.buttonNumeInregistrare.UseVisualStyleBackColor = true;
            this.buttonNumeInregistrare.Click += new System.EventHandler(this.buttonNumeInregistrare_Click);
            // 
            // buttonNumeConectare
            // 
            this.buttonNumeConectare.Location = new System.Drawing.Point(222, 63);
            this.buttonNumeConectare.Name = "buttonNumeConectare";
            this.buttonNumeConectare.Size = new System.Drawing.Size(176, 23);
            this.buttonNumeConectare.TabIndex = 6;
            this.buttonNumeConectare.Text = "Conectare";
            this.buttonNumeConectare.UseVisualStyleBackColor = true;
            this.buttonNumeConectare.Click += new System.EventHandler(this.buttonNumeConectare_Click);
            // 
            // textBoxNumeConectare
            // 
            this.textBoxNumeConectare.Location = new System.Drawing.Point(222, 12);
            this.textBoxNumeConectare.Name = "textBoxNumeConectare";
            this.textBoxNumeConectare.Size = new System.Drawing.Size(176, 20);
            this.textBoxNumeConectare.TabIndex = 4;
            // 
            // textBoxParolaInregistrare
            // 
            this.textBoxParolaInregistrare.Location = new System.Drawing.Point(12, 38);
            this.textBoxParolaInregistrare.Name = "textBoxParolaInregistrare";
            this.textBoxParolaInregistrare.Size = new System.Drawing.Size(176, 20);
            this.textBoxParolaInregistrare.TabIndex = 7;
            // 
            // textBoxParolaConectare
            // 
            this.textBoxParolaConectare.Location = new System.Drawing.Point(222, 38);
            this.textBoxParolaConectare.Name = "textBoxParolaConectare";
            this.textBoxParolaConectare.Size = new System.Drawing.Size(176, 20);
            this.textBoxParolaConectare.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 98);
            this.Controls.Add(this.textBoxParolaConectare);
            this.Controls.Add(this.textBoxParolaInregistrare);
            this.Controls.Add(this.buttonNumeConectare);
            this.Controls.Add(this.textBoxNumeConectare);
            this.Controls.Add(this.buttonNumeInregistrare);
            this.Controls.Add(this.textBoxNumeInregistrare);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Autentificare";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumeInregistrare;
        private System.Windows.Forms.Button buttonNumeInregistrare;
        private System.Windows.Forms.Button buttonNumeConectare;
        private System.Windows.Forms.TextBox textBoxNumeConectare;
        private System.Windows.Forms.TextBox textBoxParolaInregistrare;
        private System.Windows.Forms.TextBox textBoxParolaConectare;
    }
}

