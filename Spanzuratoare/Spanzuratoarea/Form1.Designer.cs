﻿namespace Spanzuratoarea
{
    partial class Spanzuratoare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxSpanzuratoare = new System.Windows.Forms.PictureBox();
            this.textBoxCuvantDat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxLitera = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonLitera = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCuvant = new System.Windows.Forms.Button();
            this.textBoxCuvantUser = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpanzuratoare)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxSpanzuratoare
            // 
            this.pictureBoxSpanzuratoare.InitialImage = null;
            this.pictureBoxSpanzuratoare.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxSpanzuratoare.Name = "pictureBoxSpanzuratoare";
            this.pictureBoxSpanzuratoare.Size = new System.Drawing.Size(236, 214);
            this.pictureBoxSpanzuratoare.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSpanzuratoare.TabIndex = 0;
            this.pictureBoxSpanzuratoare.TabStop = false;
            // 
            // textBoxCuvantDat
            // 
            this.textBoxCuvantDat.Location = new System.Drawing.Point(257, 78);
            this.textBoxCuvantDat.Name = "textBoxCuvantDat";
            this.textBoxCuvantDat.ReadOnly = true;
            this.textBoxCuvantDat.Size = new System.Drawing.Size(269, 20);
            this.textBoxCuvantDat.TabIndex = 100;
            this.textBoxCuvantDat.TabStop = false;
            this.textBoxCuvantDat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(254, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Introdu o litera";
            // 
            // textBoxLitera
            // 
            this.textBoxLitera.Location = new System.Drawing.Point(344, 106);
            this.textBoxLitera.Name = "textBoxLitera";
            this.textBoxLitera.Size = new System.Drawing.Size(98, 20);
            this.textBoxLitera.TabIndex = 0;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(448, 203);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonLitera
            // 
            this.buttonLitera.Location = new System.Drawing.Point(448, 104);
            this.buttonLitera.Name = "buttonLitera";
            this.buttonLitera.Size = new System.Drawing.Size(78, 23);
            this.buttonLitera.TabIndex = 1;
            this.buttonLitera.Text = "Trimite";
            this.buttonLitera.UseVisualStyleBackColor = true;
            this.buttonLitera.Click += new System.EventHandler(this.buttonLitera_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(254, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Introdu cuvantul";
            // 
            // buttonCuvant
            // 
            this.buttonCuvant.Location = new System.Drawing.Point(448, 158);
            this.buttonCuvant.Name = "buttonCuvant";
            this.buttonCuvant.Size = new System.Drawing.Size(75, 23);
            this.buttonCuvant.TabIndex = 3;
            this.buttonCuvant.Text = "Trimite";
            this.buttonCuvant.UseVisualStyleBackColor = true;
            this.buttonCuvant.Click += new System.EventHandler(this.buttonCuvant_Click);
            // 
            // textBoxCuvantUser
            // 
            this.textBoxCuvantUser.Location = new System.Drawing.Point(344, 132);
            this.textBoxCuvantUser.Name = "textBoxCuvantUser";
            this.textBoxCuvantUser.Size = new System.Drawing.Size(182, 20);
            this.textBoxCuvantUser.TabIndex = 2;
            this.textBoxCuvantUser.UseSystemPasswordChar = true;
            // 
            // Spanzuratoare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(531, 235);
            this.Controls.Add(this.textBoxCuvantUser);
            this.Controls.Add(this.buttonCuvant);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonLitera);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.textBoxLitera);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxCuvantDat);
            this.Controls.Add(this.pictureBoxSpanzuratoare);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Spanzuratoare";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spanzuratoare";
            this.Load += new System.EventHandler(this.Spanzuratoare_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpanzuratoare)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxSpanzuratoare;
        private System.Windows.Forms.TextBox textBoxCuvantDat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxLitera;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonLitera;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCuvant;
        private System.Windows.Forms.TextBox textBoxCuvantUser;
    }
}

