﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spanzuratoarea
{
    public partial class Spanzuratoare : Form
    {
        public Spanzuratoare()
        {
            InitializeComponent();
        }

        private void Spanzuratoare_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(System.IO.Directory.GetCurrentDirectory());
            gasesteCaiPoze();
            pictureBoxSpanzuratoare.Image = Image.FromFile(caiSprePoze[0]);
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        char[] cuvantCuAsterix = new char[50];
        char[] cuvant;
        int length;

        void initializareCuvantCuAsterix(int lungime)
        {
            for (int i = 0; i < lungime; i++)
                cuvantCuAsterix[i] = '*';
        }

        bool cautLitera(char c)
        {
            bool schimbare = false;
            int lungime = cuvant.Length;
            for (int i = 0; i < lungime; i++)
                if (cuvant[i] == c)
                {
                    cuvantCuAsterix[i] = c;
                    schimbare = true;
                }
            return schimbare;
        }

        private void buttonCuvant_Click(object sender, EventArgs e)
        {
            cuvant = textBoxCuvantUser.Text.ToCharArray();
            char pl = cuvant[0];
            char ul = cuvant[cuvant.Length - 1];
            length = cuvant.Length;
            initializareCuvantCuAsterix(length);
            
            cautLitera(pl);
            cautLitera(ul);
            textBoxCuvantUser.Text = "";
            textBoxCuvantDat.Text = new string(cuvantCuAsterix);
        }

        string[] caiSprePoze;

        void gasesteCaiPoze()
        {
            caiSprePoze = System.IO.Directory.GetFiles(@"..\..\assets\");
        }
        int greseli = 0;

        void reset()
        {
            for(int i = 0; i<length; i++)
            {
                cuvant[i] = '\0';
                cuvantCuAsterix[i] = '\0';
            }
            textBoxLitera.Clear();
            textBoxCuvantDat.Clear();
            greseli = 0;
            pictureBoxSpanzuratoare.Image = Image.FromFile(caiSprePoze[0]);
        }

        private void buttonLitera_Click(object sender, EventArgs e)
        {
            //de verificat daca cuvantul nou difera de cuvantul vechi
            //daca difera schimbam poza
            //daca nu difera zicem ca a facut bine
            //daca a ajuns la cuvantul bun => a castigat
            //daca nu mai am poze => ma opresc
            char litera = textBoxLitera.Text[0];
            
            

            if(cautLitera(litera) == false)
            {
                greseli++;
                pictureBoxSpanzuratoare.Image = Image.FromFile(caiSprePoze[greseli]);
            }
            else
                textBoxCuvantDat.Text = new string(cuvantCuAsterix);

            if (!textBoxCuvantDat.Text.Contains('*'))
            {
                MessageBox.Show("Bravo ai ghicit cuvantul");
                reset();
            }

            if(greseli >= 6)
            {
                MessageBox.Show("Ai pierdut, cuvantul era " + new string(cuvant));
                reset();
            }
        }
    }
}
