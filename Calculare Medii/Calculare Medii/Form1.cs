﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculare_Medii
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int[] note = new int[15];
        int nrNote = 0;
        int teza;
        void verificare(String nota)
        {
            if (nota == "")
                return;
            note[nrNote++] = Convert.ToInt32(nota);
        }
        
        int calculeazaMedie()
        {
            double suma = 0;
            for (int i = 0; i < nrNote; i++)
                suma += note[i];
            return Convert.ToInt32(Math.Round(((suma / nrNote) * 3 + teza) / 4));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            verificare(textBoxGG.Text);
            verificare(textBoxGB.Text);
            verificare(textBoxTG.Text);
            verificare(textBoxBIP.Text);
            verificare(textBoxB.Text);
            verificare(textBoxS.Text);
            verificare(textBoxP.Text);
            verificare(textBoxR.Text);
            verificare(textBoxPB.Text);
            if (textBoxT.Text != "")
                teza = Convert.ToInt32(textBoxT.Text);
            labelMedie.Text = Convert.ToString( calculeazaMedie() );
        }
    }
}
