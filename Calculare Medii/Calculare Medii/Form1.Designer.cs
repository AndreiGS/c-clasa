﻿namespace Calculare_Medii
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCalc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelMedie = new System.Windows.Forms.Label();
            this.textBoxGB = new System.Windows.Forms.TextBox();
            this.textBoxB = new System.Windows.Forms.TextBox();
            this.textBoxBIP = new System.Windows.Forms.TextBox();
            this.textBoxGG = new System.Windows.Forms.TextBox();
            this.textBoxTG = new System.Windows.Forms.TextBox();
            this.textBoxS = new System.Windows.Forms.TextBox();
            this.textBoxP = new System.Windows.Forms.TextBox();
            this.textBoxR = new System.Windows.Forms.TextBox();
            this.textBoxT = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxPB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonCalc
            // 
            this.buttonCalc.Location = new System.Drawing.Point(71, 314);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(75, 23);
            this.buttonCalc.TabIndex = 0;
            this.buttonCalc.Text = "Calculare";
            this.buttonCalc.UseVisualStyleBackColor = true;
            this.buttonCalc.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Grile back";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Back";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Back in plan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Grile graf";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Teorie graf";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Surprize";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Pedepse";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Recompense";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Teza";
            // 
            // labelMedie
            // 
            this.labelMedie.AutoSize = true;
            this.labelMedie.Location = new System.Drawing.Point(89, 281);
            this.labelMedie.Name = "labelMedie";
            this.labelMedie.Size = new System.Drawing.Size(36, 13);
            this.labelMedie.TabIndex = 10;
            this.labelMedie.Text = "Media";
            // 
            // textBoxGB
            // 
            this.textBoxGB.Location = new System.Drawing.Point(96, 8);
            this.textBoxGB.Name = "textBoxGB";
            this.textBoxGB.Size = new System.Drawing.Size(100, 20);
            this.textBoxGB.TabIndex = 11;
            // 
            // textBoxB
            // 
            this.textBoxB.Location = new System.Drawing.Point(96, 34);
            this.textBoxB.Name = "textBoxB";
            this.textBoxB.Size = new System.Drawing.Size(100, 20);
            this.textBoxB.TabIndex = 12;
            // 
            // textBoxBIP
            // 
            this.textBoxBIP.Location = new System.Drawing.Point(96, 60);
            this.textBoxBIP.Name = "textBoxBIP";
            this.textBoxBIP.Size = new System.Drawing.Size(100, 20);
            this.textBoxBIP.TabIndex = 13;
            // 
            // textBoxGG
            // 
            this.textBoxGG.Location = new System.Drawing.Point(96, 86);
            this.textBoxGG.Name = "textBoxGG";
            this.textBoxGG.Size = new System.Drawing.Size(100, 20);
            this.textBoxGG.TabIndex = 14;
            // 
            // textBoxTG
            // 
            this.textBoxTG.Location = new System.Drawing.Point(96, 112);
            this.textBoxTG.Name = "textBoxTG";
            this.textBoxTG.Size = new System.Drawing.Size(100, 20);
            this.textBoxTG.TabIndex = 15;
            // 
            // textBoxS
            // 
            this.textBoxS.Location = new System.Drawing.Point(96, 138);
            this.textBoxS.Name = "textBoxS";
            this.textBoxS.Size = new System.Drawing.Size(100, 20);
            this.textBoxS.TabIndex = 16;
            // 
            // textBoxP
            // 
            this.textBoxP.Location = new System.Drawing.Point(96, 164);
            this.textBoxP.Name = "textBoxP";
            this.textBoxP.Size = new System.Drawing.Size(100, 20);
            this.textBoxP.TabIndex = 17;
            // 
            // textBoxR
            // 
            this.textBoxR.Location = new System.Drawing.Point(96, 190);
            this.textBoxR.Name = "textBoxR";
            this.textBoxR.Size = new System.Drawing.Size(100, 20);
            this.textBoxR.TabIndex = 18;
            // 
            // textBoxT
            // 
            this.textBoxT.Location = new System.Drawing.Point(96, 242);
            this.textBoxT.Name = "textBoxT";
            this.textBoxT.Size = new System.Drawing.Size(100, 20);
            this.textBoxT.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 219);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Pbinfo";
            // 
            // textBoxPB
            // 
            this.textBoxPB.Location = new System.Drawing.Point(96, 216);
            this.textBoxPB.Name = "textBoxPB";
            this.textBoxPB.Size = new System.Drawing.Size(100, 20);
            this.textBoxPB.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(218, 348);
            this.Controls.Add(this.textBoxPB);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxT);
            this.Controls.Add(this.textBoxR);
            this.Controls.Add(this.textBoxP);
            this.Controls.Add(this.textBoxS);
            this.Controls.Add(this.textBoxTG);
            this.Controls.Add(this.textBoxGG);
            this.Controls.Add(this.textBoxBIP);
            this.Controls.Add(this.textBoxB);
            this.Controls.Add(this.textBoxGB);
            this.Controls.Add(this.labelMedie);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCalc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCalc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelMedie;
        private System.Windows.Forms.TextBox textBoxGB;
        private System.Windows.Forms.TextBox textBoxB;
        private System.Windows.Forms.TextBox textBoxBIP;
        private System.Windows.Forms.TextBox textBoxGG;
        private System.Windows.Forms.TextBox textBoxTG;
        private System.Windows.Forms.TextBox textBoxS;
        private System.Windows.Forms.TextBox textBoxP;
        private System.Windows.Forms.TextBox textBoxR;
        private System.Windows.Forms.TextBox textBoxT;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxPB;
    }
}

